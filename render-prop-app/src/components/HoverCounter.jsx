import React, { Component } from "react";

class HoverCounter extends Component {
  render() {
    const { count, incrementCount, clearCount } = this.props;
    return (
      <div>
        <h1 onMouseOver={incrementCount}> Hovered {count} times</h1>
        <button onClick={clearCount}>Clear</button>
      </div>
    );
  }
}

export default HoverCounter;
