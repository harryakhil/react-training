import React, { Component } from "react";

class Counter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      count: 0
    };
  }

  incrementCount = () => {
    this.setState(prevState => {
      return { count: prevState.count + 1 };
    });
  };
  
  clearCount = () => {
    this.setState({ count: 0 });
  };

  render() {
    return (
      <>
        {this.props.children(
          this.state.count,
          this.incrementCount,
          this.clearCount
        )}
      </>
    );
  }
}

export default Counter;
