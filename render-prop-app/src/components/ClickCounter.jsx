import React, { Component } from "react";

class ClickCounter extends Component {
  render() {
    const { count, incrementCount, clearCount } = this.props;
    return (
      <div>
        <button onClick={incrementCount}> clicked {count} times</button>&nbsp;
        <button onClick={clearCount}>Clear</button>
      </div>
    );
  }
}

export default ClickCounter;
