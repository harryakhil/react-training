const commonStyles = {
  default: {
    backgroundColor: "#28a745",
    color: "#eae8e8",
    padding: "10px",
    borderRadius: "5px"
  },
  disable: {
    backgroundColor: "#9c9c9c",
    color: "#c7c6c6"
  }
};

export default commonStyles;
