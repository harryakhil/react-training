import React from 'react';
import commonStyles from '../styles';

const translateProps = (props) => {
    let _styles = {...commonStyles.default};
    if(props.disable) {
        _styles = {..._styles, ...commonStyles.disable};
    }
    const newProps = {...props, styles: _styles};
    return newProps;
}
export default (wrapperComponent) => {
    return function wrapperRender(args) {
        return wrapperComponent(translateProps(args));
    }
};