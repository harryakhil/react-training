import React from "react";
import "bootstrap/dist/css/bootstrap.css";
// import ButtonOne from "./components/ButtonOne";
import ClickCounter from "./components/ClickCounter";
import HoverCounter from "./components/HoverCounter";
import Counter from "./components/Counter";
// import Amount from './Amount';

const App = () => (
  <div className={"container"}>
    <center>
      <h1>Currency Converter</h1>
    </center>
    <hr />
    <div className={"row"}>
      {/* <Amount /> */}
      <div className={"col-sm-4"}>&nbsp;</div>
      <div className={"col-xs-4"}>
        {/* HOC component */}
        {/* <ButtonOne disable/> */}
        {/* <ClickCounter /> */}
        {/* <HoverCounter /> */}
        <Counter>
          {(count, incrementCount, clearCount) => (
            <ClickCounter count={count} incrementCount={incrementCount} clearCount={clearCount}/>
          )}
        </Counter>
        <Counter>
          {(count, incrementCount, clearCount) => (
            <HoverCounter count={count} incrementCount={incrementCount} clearCount={clearCount}/>
          )}
        </Counter>
      </div>
    </div>
  </div>
);

export default App;
